# YFWP, Yi Fixed Width Parser

YFWP is a library help to deal with Fixed Width text files.

## Usage

### Installation
    go get https://github.com/gonyi/yfwp

### Importing the library
    import "https://github.com/gonyi/yfwp"

### Initializing
    data := yfwp.New()

### Define
    data.AddField("Name", 1, 10) // length 1~10 will be name.
    data.AddField("Age", 11, 13) // remember it starts with 1 not 0.

### Usage
    fmt.Println(data.Field("Name"), data.Field("Age"))
    // all the data is stored as hash table with string type key and string type value.

### License

Copyright (c) 2015 Gon Yi

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
