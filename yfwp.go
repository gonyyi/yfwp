/* --------------------------------------------------------------------------- *
  Title:		yfwp 1.1 (Yi Fixed Width Parser)

  Author:		Gon Yi (gonyispam@gmail.com)
  Date:			July 20, 2015
  Description:	This parses Fixed Width string to a map
  		v1.1: add function "AddFieldByName"
* --------------------------------------------------------------------------- */

/*
package main

import (
	"fmt"
	"github.com/gonyi/yfwp"
)

func main() {
	data := yfwp.New()
	data.AddField("Name", 1, 10)
	data.AddField("Age", 11, 13)
	data.SetData("GON       1  ")
	fmt.Println(data.Field("Name"), data.Field("Age"))
	data.SetData("ACX       1  ")
	fmt.Println(data.Field("Name"), data.Field("Age"))
}
*/

package yfwp

import (
	"errors"
	"os"
	"bufio"
	"strconv"
	"strings"
)

type fwp struct {
	items        map[string]fwpItem
	largestRange int
	PreferedOrder []string
}

type fwpItem struct {
	Start int
	End   int
	Value string
}

func New() fwp {
	out := fwp{items: make(map[string]fwpItem), largestRange: 0, PreferedOrder: []string{}}
	return out
}

func (f *fwp) AddFieldByFile(filename string) error {
	fi,err:=os.Open(filename)
	if err!=nil {
		return err		
	}
	defer fi.Close()
	bfi:=bufio.NewScanner(fi)
	
	for bfi.Scan() {
		lineStr := bfi.Text()
		aLineStr := strings.Split(lineStr,",")
		if len(aLineStr)<3 {
			return errors.New("Input should have at least 3 fields of comma delimited; [1]Name, [2]Start, [3]End, then ignored")
		}
		if len(aLineStr[0])==0 {
			return errors.New("Field name missing")
		}
		start, err:= strconv.Atoi(strings.TrimSpace(aLineStr[1]))
		if err!=nil {
			return errors.New("Start range should be a number")
		}
		end, err:= strconv.Atoi(strings.TrimSpace(aLineStr[2]))
		if err!=nil {
			return errors.New("End range should be a number")
		}
		f.AddField(aLineStr[0], start, end)
		f.PreferedOrder = append(f.PreferedOrder, aLineStr[0])
	}
	return nil
}

func (f *fwp) AddField(name string, start, end int) error {
	if end > f.largestRange {
		f.largestRange = end
	}
	if start > end {
		return errors.New("START value cannot be equal to or grater than END value")
	}
	if _, ok := f.items[name]; !ok {
		f.items[name] = fwpItem{Start: start, End: end}
	}
	return nil
}

func (f *fwp) SetData(data string) error {
	if len(data) < f.largestRange {
		return errors.New("Data is smaller than expected")
	}
	for a, b := range f.items {
		f.items[a] = fwpItem{
			Start: b.Start,
			End:   b.End,
			Value: data[(b.Start - 1):b.End],
		}
	}
	return nil
}

func (f *fwp) Field(fieldName string) string {
	return f.items[fieldName].Value
}

func (f *fwp) ShowData() map[string]fwpItem {
	return f.items
}
